from django.http import HttpResponse
import datetime
from django.shortcuts import render

from NWA.db_operations.users import do_sign_in
from NWA.db_operations.users import do_register
from NWA.db_operations.users import do_logout

from NWA.db_operations.categories import do_add_category
from NWA.db_operations.questions import do_add_question
from NWA.db_operations.questions import do_show_qa

from NWA.db_operations.db_config import auth


#Here we are creating our views

def postsign(request):
	if request.method == 'POST':
		email=request.POST.get("email", "")
		password = requset.POST.get('pass')
		now = datetime.datetime.now()
		user = auth().sign_in_with_email_and_password(email,password)
		if user is not None:
			login(request, user)
			html = "<html><body>It is now %s.</body></html>" % now
		# Redirect to a success page.
			#return render(request,"home.html",{'err': "AAAA"})
		else:
		# Return an 'invalid login' error message.
			#return render(request,"home.html",{'er': "BBBB"})
			return HttpResponse(html)


def login_register(request):
	return render(request,'login_register.html',{'nbar': 'login_register'})

def about(request):
	return render(request,'about.html',{'nbar': 'about'})

def home(request):
	return render(request,'home.html',{'nbar': 'home'})

def nav_template(request):
	return render(request,'nav_template.html')

def faq(request):
	return render(request,'faq.html',{'nbar': 'faq'})

def contact(request):
	return render(request,'contact.html',{'nbar': 'contact'})

def logged_out(request):
	return render(request,'logged_out.html',{'nbar': 'logged_out'})

################################################################################
def postsign(request):
    if "sign_in" in request.POST:
        return do_sign_in(request)
    elif "register" in request.POST:
        return render(request, 'register.html', {'nbar': 'signed_in'})
################################################################################

################################################################################
def register(request):
    return do_register(request)
################################################################################
def sigin(request):
    return do_sign_in(request)

################################################################################
def logout(request):
    return do_logout(request)

################################################################################
def add_question(request):
    return do_add_question(request)
################################################################################

################################################################################
def add_category(request):
    return do_add_category(request)
################################################################################

################################################################################
def show_qa(request):
    return do_show_qa(request)
################################################################################
