1.	Dacă sunt bursier Erasmus în semestrul 1 și sunt deja plecat(ă) din țară în perioada depunerii contractelor anuale de studii, ce pot face pentru a fi înscris(ă) în an?
R. Studenții aflați în această situație pot lăsa reprezentantului de an exemplarul în original (semnat) pentru a fi depus la secretariat sau pot trimite secretarului de an responsabil pentru secția/programul din care fac parte copia scanată a contractului de studii (semnat), urmând ca la întoarcerea în țară să aducă contractul în original (semnat).

2.	Dacă voi fi bursier Erasmus în străinătate, am dreptul la burse acordate de USV?
R. DA. Pe parcursul stagiului Erasmus studenți beneficiază în continuare de bursa de merit și de bursa socială obținută pentru anul academic în care desfășoară stagiul Erasmus.

3.	În ce condiții am dreptul la cazare în căminele USV?
R. Universitatea "Stefan cel Mare" din Suceava deține 4 cămine studențești. În acestea pot fi cazați studenți care urmează cursurile unei facultăți din cadrul universității, masteranzi, doctoranzi, precum și profesori sau personal administrativ - în limita locurilor rămase disponibile. 
Solicitanții pentru cazări ocazionale sau participanții la manifestările științifice organizate de facultate pot fi cazați în camere oficiale existente în căminul C4 (31 cam. x 2locuri/cam.), sau în cele 3 apartamente de protocol din căminul C2 (compuse din living, dormitor și grup sanitar). 
La acest link http://www.eed.usv.ro/ eed_files/regulamente/ 2013/Hotarare_CF_ criterii_repartizare_locuri_cazare.pdf,  veți găsi criteriile de departajare.

4.	Dacă voi fi bursier Erasmus în străinătate, am dreptul la cazare în căminele USV?
R. DA, pentru semestrul de studii petrecut la Suceava în anul universitar în care este efectuat stagiul Erasmus. Conform procedurii de cazare a studenților Erasmus, cei plecați în stagiul Erasmus în semestrul 1 și care se reîntorc în semestrul 2, trebuie să trimită în luna decembrie un email pe adresa social@usv.ro , specificând data întoarcerii (data de la care este necesară cazarea în cămin) și opțiunea pentru un anumit cămin. Cei plecați în stagiul Erasmus în semestrul 2 și care au cazare în cămin vor schimba camerele cu cei care revin din stagiul Erasmus.  Află mai multe despre Bursele Erasmus.

5.	In perioada înscrierilor la facultate este program si in perioada week-end-ului?
SESIUNEA IULIE
R. ÎNSCRIERE    				9 - 19 iulie, fără sâmbătă si duminică 
AFIŞARE REZULTATE    			20 iulie, orele 08.00 – 13.00 
DEPUNERE CONTESTATII      	 	20 iulie, orele 13.00 – 16.00 
AFISARE REZULTATE CONTESTATII    23 iulie 
ÎNMATRICULARI        			Calculatoare 23-25 iulie; 								celelalte programe 23-26 iulie 
AFISARE REZULTATE FINALE    27 iulie

6.	La înscriere trebuie sa mă prezint si cu actele in original si cu copii conform cu originalul?
Da.

7.	Cum știu ce notă am obținut?
R. Notele sunt accesibile online din platforma disponibilă prin contul USV. Pentru întrebări privind accesarea platformei online vă puteți adresa Serviciul de Comunicații și Tehnologii Informaționale la http://dcti.usv.ro/ unde veți găsi toate datele necesare. La sfârșitul fiecărei sesiuni, notele sunt afișate timp de o lună și la avizierul USV. Profesorii pot comunica notele cursanților lor, dar nu sunt obligați să o facă.

8.	Cred că am obținut altă notă decât cea afișată online sau la avizier. Cui mă adresez?
R. Într-o astfel de situație, vă veți adresa secretarului de an responsabil pentru secția/programul din care faceți parte, împreună cu carnetul de student unde a fost trecută nota de către profesorul examinator. Dacă se constată că nota trecută în catalog este diferită de cea afișată online se vor opera modificări în platforma online. Dacă nota din carnet nu este identică cu cea din catalog sau nu există notă trecută în carnet și, în baza comunicării dintre student și profesor, studentul este îndreptățit să creadă (i.e. are o dovadă scrisă) că nota obținută este diferită de cea trecută în catalog, studentul va lua legătura cu profesorul examinator care va opera ulterior eventualele modificări în catalog, iar apoi Secretariatul va opera modificările necesare în platforma online.

9.	De câte ori am dreptul să susțin un examen?
R. Pentru cursurile nepromovate se pot susține examene restante:
- Pentru un curs din anul curent se pot susține în anul curent maxim trei examene (prima examinare și două reexaminări)
- Prima reexaminare este gratuită;
- A doua reexaminare se plătește;
- Pentru un curs din ani anteriori, examenul restant poate fi susținut o singură dată în fiecare an și se plătește:
- Pentru toate cursurile din anul curent, studenții pot susține, în ultima sesiune din anul curent, examene în vederea măririi notei (pentru maxim trei cursuri).

10.	Câte restanțe pot susține într-o sesiune?
Nelimitat

11.	În ce sesiuni am dreptul să susțin examene restante sau de diferență?
R. Dreptul de a susține examene restante depinde de parcursul studentului (parcurs normal, student reînmatriculat, student transferat, student cu semestre suplimentare de finalizare a studiilor) și de semestrul în care a avut loc cursul, astfel
•	În sesiunea de iarnă (ianuarie-februarie), pot fi susținute următoarele tipuri de examene restante
o	examene restante de semestrul I din anii anteriori, exclusiv pentru studenții aflați în an terminal (an III licență / an II master)
-	la cerere și achitând taxa aferentă fiecărui examen restant; 
-	cererile se depun personal la secretariat, în timpul programului de lucru; 
-	cererea de înscriere pentru susținerea examenelor restante va fi însoțită obligatoriu de chitanța doveditoare de achitare a taxei pentru examenele restante. 
o	studenții cu semestru suplimentar pentru finalizarea studiilor ("semestru de grație")
-	studenții aflați în această situație vor putea susține examenele, o singură dată, în această sesiune;
-	studentul care nu reușește să promoveze examenele în această sesiune va fi exmatriculat;
-	examenele care figurează în planul de învățământ în semestrul II, vor putea fi programate în această sesiune doar cu acordul cadrului didactic; în eventualitatea în care cadrul didactic nu își dă acordul, studentul va solicita un al doilea semestru suplimentar, cu taxă, dar numai pentru acest(e) examen(e).
o	 studenții reînmatriculați în anul III
-	pentru examenele de diferență se depune obligatoriu cerere și se plătește fiecare examen socotit a fi de diferență; 
-	în această sesiune de examene din iarnă se susțin doar examenele de diferență din semestrul I; 
-	examenele de diferență din anii I și/sau II, pentru cursurile din semestrul II se vor susține în sesiunea de examene din vară (01.06 – 29.06. 2014); 
-	examenele de diferență se pot susține o singură dată și anume în anul în care studentul a fost reînmatriculat sau transferat; 
-	studentul care nu reușește să promoveze examenele de diferență în anul în care a fost reînmatriculat sau transferat va fi exmatriculat. 

12.	În ce sesiuni am dreptul să particip la reexaminare în vederea măririi notei?
R. Un examen poate fi susținut în sesiunea III și pentru MĂRIREA NOTEI cu condiția de integralitate după sesiunea din vară. Notele acordate la proiecte nu mai pot fi mărite.

13.	Pot susține examene de mărire a notei dacă am examene restante?
R. Un examen poate fi susținut în sesiunea III și pentru MĂRIREA NOTEI cu condiția de integralitate după sesiunea din vară.

14.	Pot susține examene de mărire a notei pentru examene din alți ani?
R. NU. Reexaminările în vederea măririi notei pot fi susținute exclusiv pentru cursurile din anul curent academic.

15.	Care sunt actele necesare înscrierii la examenul de licență?
R. Conținutul dosarului de înscriere la examenul de licență este precizat în secțiunea Educație (licență), ce poate fi accesată fie din http://www.eed.usv.ro/fiesc/html/educatie.html, fie din http://scolaritate.usv.ro/ > http://finalizarestudii.usv.ro/ > in meniu la secțiunea Examene Diploma/Disertație.

16.	Care sunt actele necesare înscrierii la examenul de disertație?
R. Conținutul dosarului de înscriere la examenul de licență este precizat în secțiunea Educație (licență), ce poate fi accesată fie din http://www.eed.usv.ro/fiesc/html/educatie.html, fie din http://scolaritate.usv.ro/ > http://finalizarestudii.usv.ro/ > in meniu la Examene Diploma/Disertație.

17.	Am absolvit USV într-o promoție anterioară. Cum mă pot înscrie la examenul de licență/disertație?


18.	De cate ori pot da un examen de licență/disertație?

R. Nu este o limita, nici de timp, nici ca număr de încercări.

19.	Ce mobilități internaționale pot efectua ca student?
R. La adresa www.relint.usv.ro găsiți toate programele internaționale care sunt in desfășurare.
20.	Când are loc competiția pentru mobilități Erasmus?

R. Pentru anul universitar 2018-2019 se vor organiza selecțiile pentru studenții Erasmus după următorul calendar:
- depunerile la secretariatul facultății pana pe 3 aprilie 2018,
- selecția va fi realizata până pe 6 aprilie 2018.
Criteriile pentru selecție si lista de mobilități se regăsesc pe site-ul facultății si al universității.
Lista mobilităților Erasmus disponibile,  criteriile de eligibilitate, precum și alte informații suplimentare se pot obține de la:
- Serviciul de Relații Internaționale și Afaceri Europene (corp E, camera 013);
- coordonatorii Erasmus ai facultăților;
- http://www.usv.ro/relint/outgoing_erasmus_plus_students.php

21.	Pot obține sau efectua un stagiu Erasmus dacă am examene restante?

R. NU. Unul din criteriile de eligibilitate pentru a putea participa în competiția pentru burse de mobilitate Erasmus este de a fi integralist la momentul depunerii dosarului de candidatură. De asemenea, studenții care au câștigat competiția Erasmus ca titulari sau rezerve trebuie să promoveze toate examenele pentru cursuri urmate la USV până la momentul plecării în stagiul Erasmus. Află mai multe despre condițiile de eligibilitate pentru o bursă Erasmus la www.relint.usv.ro.

22.	Unde pot obține mai multe informații despre bursele Erasmus?
R. Pe pagina web www.relint.usv.ro sau la - http://www.usv.ro/relint/outgoing_erasmus_plus_students.php.
23.	Nu îmi pot accesa contul FSPUB, probabil mi-am uitat parola. Cui mă adresez?
R. La Serviciul de Comunicații și Tehnologii Informaționale de la link-ul http://www.dcti.usv.ro unde veți găsi toate datele necesare.
24.	Policlinica studențească: unde se găsește și care este orarul?
R. CABINET MEDICAL -Căminul C1 parter Tel: int. 186
DR. BERNARD GRIMBERG	DR. ELLA IAZ
Luni	13:30 – 19:00	Luni	8:00 - 13:00
Marți	13:30 - 19:00	Marți	8:00 - 13:00
Miercuri	8:00 - 13:00	Miercuri	13:30 – 19:00
Joi		Joi	8:00 - 13:00
Vineri	8:00 - 13:00	Vineri	13:30 -19:00

25.	Mi-am pierdut carnetul / legitimația de transport, ce trebuie să fac?
R. În cazul pierderii documentelor personale (carnet de student, legitimație de transport) se eliberează un duplicat la Secretariatul FIESC după anunțarea în presă a pierderii și achitarea taxei de duplicat. În presa scrisă locală trebuie să apară un anunț de forma: "Pierdut carnet student / legitimație transport pe numele ..., eliberat(a) de Universitatea "Stefan cel Mare" din Suceava, pe care îl declar nul.", care poate fi comandat online sau la redacțiile ziarelor. Un exemplar din ziarul cu anunțul este adus la Secretariat, alături de o fotografie pentru document și o cerere pentru eliberarea acestuia.
26.	Ce este special în legătură cu orele de educație fizică?
R. Disciplina "Educație fizică" este obligatorie în contractul de studii pentru o perioadă de 4 semestre. Chiar dacă nu este prevăzute cu credite, iar nota nu se adaugă la media generală, nu vei putea să intri în examenele de finalizare a studiilor de licență dacă nu ai parcurs complet această disciplină. Pentru cei scutiți de orele de educație fizică: perioada înscrisă pe scutire trebuie să fie de un semestru / un an pentru o scutire pe întreaga durată (scutirile pentru câteva zile / săptămâni nu sunt luate în considerare). Originalul adeverinței de scutire trebuie prezentat la Policlinică studențească pentru avizare, apoi la Secretariatul FIESC, iar o copie se prezintă cadrului didactic de disciplină.
27.	Am fost bolnav, dar am o scutire de la medicul de familie pentru a justifica acest lucru. Ce fac cu scutirea?
R.  Pentru scutirea de absențe în timpul perioadei de boală, originalul adeverinței trebuie prezentat întâi Policlinicii studențești pentru avizare, mai apoi să te întâlnești cu profesorul responsabil de program de studii (sau directorul de departament) pentru a lua la cunoștință acest fapt, iar la final, adeverința / scutirea trebuie adusă la Secretariatul FIESC pentru arhivare la dosar.
28.	Când și cum pot avea reducere la abonamentul de transport în comun?
R. Decontarea biletelor de transport auto urban și interurban, în cuantum de 50% din valoare se face dacă sunt îndeplinite următoarele condiții și criterii:
1. se decontează 50% din valoarea abonamentului pe mijloace de transport auto urban și interurban, cu excepția vacanțelor universitare;
2. decontarea a 50% din contravaloarea a două bilete de călătorie cu alte mijloace decât calea ferată în cazul vacanțelor universitare din care unul la ducere și altul la întoarcere;
3. decontarea a 50% din valoarea biletelor de călătorie cu alte mijloace transport, decât calea ferată să se facă numai pentru localitățile unde nu există mijloace C.F.R.;
4. biletele de călătorie cu alte mijloace decât calea ferată în perioada de activități de învățământ să poată fi decontată în proporție de 50% din valoare numai cu aprobarea prealabilă a decanului facultății, a directorului colegiului sau departamentului;
6. pentru decontare studenții vor depune biletele până la data de 5 a lunii următoare pentru luna care a expirat conform modelului de decont la secretariatele facultăților, colegiilor sau departamentelor;
7. nu vor fi admise la decontare bilete care nu au înscrise data de către emitent, cu modificări, pe care nu este înscris traseul de deplasare ori din alte perioade de timp decât cele prevăzute la punctul 5.

29.	Nu am acumulat toate creditele obligatorii după un semestru / an, ce se întâmplă acum?
R. Neacumularea tuturor creditelor ECTS pentru disciplinele obligatorii din diferite motive (prezente insuficiente, nereușita unui examen la o disciplină în sesiunea de restanțe) îți schimbă statutul din student integralist în student neintegralist. Doar studenții integraliști au prioritate la locurile bugetate la redistribuiri (în ordinea descrescătoare a mediilor), dacă rămân locuri neocupate la buget după clasificarea studenților integraliști, atunci acestea vor fi ocupate de studenții neintegralişti în ordinea mediilor. Totodată, studenții neintegraliști nu au dreptul la bursă, iar șansele de a primi un loc în cămin sunt minime. Prin urmare, grijă la prezențele necesare la ore și notele de trecere (cel puțin) din timpul sesiunii ordinare sau de restanțe. Pentru a putea intra în examenele de finalizare a studiilor de licență, trebuie să fii integralist în momentul înscrierii în examene (luna iunie).
30.	Nu am toate notele trecute în tabelul de notare de la situația școlară din contul personal la final de semestru / am căsuțe goale, ce pot să fac?
R. Odată ce Secretariatul FIESC publică anunțul de verificare a conturilor personale pentru eventuale nereguli, caută cu atenție discordanțele între notele cunoscute și datele înscrise în tabel. Dacă observi căsuțe goale sau ai nelămuriri, contactează cât de repede Secretariatul FIESC pentru clarificări. Sunt situații în care se pot strecura erori de transcriere sau neclarități din partea cadrului didactic. 

31.	Ce este special în legătură cu modulul psihopedagogic în privința creditelor și notelor?
R. În momentul contractării modului psihopedagogic, acesta devine disciplină obligatorie. Diferența este dată de faptul că notele primite în cadrul modulului nu sunt luate în calcul la media semestrială / anuală. Fiind disciplină obligatorie, lipsa creditelor din cauze de prezentă sau nepromovare examene atrage schimbarea statutului din student integralist în student neintegraliști.
32.	Cine primește bursă și când intră în cont?
R. Criterii de acordare și revizuire a burselor
            Art. 14 – Bursa de merit se acordă studenților din învățământul superior de stat, cursuri de zi, în funcție de rezultatele obținute la învățătură. Bursele de merit se revizuiesc la începutul semestrului al II-lea, în funcție de rezultatele obținute la învățătură; se acordă studenților care după sesiunea de iarnă sunt integraliști și au obținut rezultate deosebite la învățătură.
            Art. 15 – Bursa de studiu se acordă studenților  care sunt integraliști în sesiunile legale de examene (primăvară, vară, toamnă – septembrie – restanțe), indiferent de veniturile realizate de familia studentului, în funcție de fondul de burse alocat fiecărei specializări și an studiu. Studenții anului I pot beneficia de bursă de studiu în semestrul I având drept criteriu media examenului de admitere. Bursele de studiu se revizuiesc la începutul semestrului al II-lea la nivelul rezultatelor obținute la examenele semestrului I.
            Art. 16 – Bursa de ajutor social se acordă, la cerere, în limita fondurilor alocate, studenților integraliști, în funcție de veniturile nete medii lunare ce revin pe membru de familie în cazul când:
-	li se aplică prevederile art. 10, lit. r) din Legea nr. 42/1990, republicată, în baza certificatului eliberat de Comisia pentru aplicarea Legii nr. 42/1990, care atestă calitatea lor ori a unuia dintre părinți de „Luptători pentru Victoria Revoluției Române din Decembrie 1989” sau de „Erou-martir” – cu una dintre mențiunile: rănit, reținut, rănit și reținut, remarcat prin fapte deosebite, însoțit de brevet semnat de Președintele României;
-	sunt orfani de ambii părinți, provin din casele de copii sau plasament familial, și care nu realizează venituri;
-	sunt bolnavi de TBC și se află în evidenta dispensarelor medicale, suferă de diabet, boli maligne, sindrom uri de malasorbţie grave, insuficiențe renale cronice, astm bronşic, epilepsie, cardiopatii congenitale, hepatită cronică, glaucom, miopie gravă, boli imunologice, pri sunt infestați cu virusul HIV sau bolnavi de SIDA, spondilită anchilozantă sau reumatism articular acut;
-	fac dovada că venitul lunar net pe membru de familie pe ultimele 3 luni este mai mic sau egal cu salariul de bază minim pe țară;
Criterii specifice și coeficienții de importantă pentru ierarhizarea studenților care solicită bursă de ajutor social se stabilesc de către Consiliul Academic al Facultății. Cuantumul fiecărei categorii de bursă se va stabili semestrial în raport cu fondurile distribuite în acest sens pentru fiecare facultate.
            Art. 17 – Bursa de ajutor social ocazional pentru îmbrăcăminte se poate acorda, la cerere, în cuantumul unei burse de ajutor social, de două ori în decursul unui an universitar, în limita fondurilor repartizate în acest scop. Pot beneficia de bursă de ajutor social ocazională studenții defavorizați din punct de vedere social, studenții orfani de ambii părinți, proveniți din casele de copii sau din plasament familial, a căror familie nu realizează pe ultimele 3 luni un venit lunar mediu pe membru de familie mai mare de 75% din salariul minim pe economie.
            Art. 18 – Bursa de ajutor social ocazională pentru maternitate se acordă studentei sau studentului a cărui soție nu realizează alte venituri decât bursele și constă într-o bursă pentru naștere și lăuzie și o bursă pentru procurarea îmbrăcămintei copilului nou-născut.
            Art. 19 – Bursa de ajutor social ocazională în caz de deces se poate acorda pentru decesul unui membru al familiei studentului (ei) (soție, soț, copil) sau în caz de deces al studentului (ei) necăsătorit (ă) sau căsătorit (ă), cu soție/soț care nu realizează venituri.
            Art. 20 – Bursa de studiu pentru studenții cu domiciliul în  mediul rural  se acordă conform H.G. nr. 769 din 14.07.2005, și se poate cumula cu alte tipuri de bursă acordate conform legislației în vigoare.
            Art. 21 – Bursa  pentru sportivii de performanță se acordă atât studenților fără taxă cât și celor cu taxă. Bursa de performanță sportivă se acordă începând cu anul II studenților care au performante sportive deosebite (anexând listele și documentele justificative) astfel:
-	Campionatele Naționale Universitare (locurile I-III) sau la Campionatele Mondiale Universitare (locurile I-VIII);
-	La jocuri sportive au promovat și s-au menținut în primul eșalon al Campionatului National cu echipa universității și au participat mai mult de 30% din timpul total de joc;
-	Au fost selecționați la Loturile Naționale de Seniori și Tineret participând în competițiile internaționale și s-au clasat pe locurile I-VIII la Campionate Europene/Campionate Mondiale/Jocuri Olimpice la sporturi individuale (atletism, lupte, arte marțiale);
-	Studenților care s-au clasat pe locurile I-III la Campionate Naționale la sporturi individuale (atletism, lupte, arte marțiale).
Cuantumul lunar al bursei se stabilește la începutul fiecărui an universitar. Bursele se atribuie anual studenților din anul II, III și IV pentru următoarele 12 luni. Această bursă se acordă indiferent de veniturile realizate de familia studentului.
            Art. 22 - Subvenții individuale de sprijin pentru cazare se acordă conform OUG 73/2004. Pot beneficia de subvenție individuală de sprijin pentru cazare studenții care îndeplinesc următoarele condiții:
a) nu au domiciliul sau reședința în localitatea în care se află instituția de învățământ superior la care studiază;
b) provin din familii cu venituri brute lunare pe membru de familie care nu depășesc salariul minim brut pe economie;
c) au vârsta de până la 29 de ani;
d) optează pentru subvenție și nu au primit sau nu au solicitat un loc de cazare în căminele studențești ale instituției de învățământ superior în care frecventează cursurile.

33.	Unde găsesc informații despre taxe și termenele de plată?
R. La următoarele link-uri http://www.seap.usv.ro/avizier/detalii.php?bid=1456 si http://www.usv.ro/fisiere_utilizator/file/Acte%20oficiale/2017/taxe_2017-2018_01.pdf .

34.	Ce se întâmplă dacă nu achit taxa de școlarizare la termen?
R. Neplata tranșei din taxa de școlarizare în termenele convenite atrage exmatricularea de drept a studentului constatată prin ordin al rectorului universității, începând cu următoarea zi lucrătoare după termenul de plată nerespectat.

35.	Unde pot găsi contacte ale cadrelor didactice?
R. Cadrele didactice au afișate datele de contact pe pagina personală (CV-ul personal) în capitolul "Personal" de pe site.
36.	Cum funcționează universitatea?
R. Universitatea este o instituție de învățământ superior care funcționează în bază unei autonomii proprii, fiind compusă din mai multe facultăți.
La rândul ei, facultatea cuprinde unul sau mai multe domenii de specialitate, înrudite între ele, și se împarte în departamente.
Departamentul controlează programul / programele de studii sau în variantă mai populară - secția, specializarea. 

Pe scurt, student >> program de studii >> departament >> facultate >> universitate.

37.	Cine conduce universitatea și structurile acesteia?
R. Universitatea este condusă de rector, sprijinit în această activitate de 4 prorectori. Fiecare facultate este condusă de un decan, având în subordine un prodecan. Departamentul este condus de directorul de departament.  
Rectorul este ales de către toate cadrele didactice și studenții reprezentanți, iar acesta desemnează prorectorii și decanii alături de care va conduce universitatea. Directorii de departament sunt aleși în cadrul departamentului din care aceștia provin.

38.	Cum se iau deciziile în universitate?
R. O singură persoană nu poate lua decizii care să afecteze întreagă universitate sau facultate. Din acest motiv, există un Senat universitar. Senatul universitar reprezintă comunitatea universitară și este cel mai înalt for de decizie și deliberare la nivelul universității, desfășurându-și activitatea în baza și cu respectarea Cartei Universității, a Legii educației naționale, a hotărârilor Guvernului și a ordinelor ministerului de resort. Senatul universitar este alcătuit în proporție de 75% din reprezentanții personalului didactic și de cercetare titular și în proporție de 25% din reprezentanții studenților.
Senatul universitar este condus de un președinte, ales prin vot secret de către membri. Președintele Senatului conduce ședințele și reprezintă organismul condus în relația cu rectorul.
Senatul universitar poate fi convocat de către rector sau la cererea a cel puțin o treime din numărul de membri. Sesiunile Senatului universitar se desfășoară numai în prezenta a cel puțin două treimi din totalul membrilor săi. Hotărârile Senatului se iau cu votul majorității membrilor prezenți.

39.	Daca sunt student cu taxa pot beneficia de bursa de studiu?
R. În conformitate cu prevederile legislative, studenții din cadrul Universității „Stefan cel Mare” din Suceava care urmează cursuri de zi, învățământ fără taxă și cu taxă, pot beneficia de următoarele tipuri de burse și forme de sprijin material:
- burse de merit;
- burse de studiu;
- burse sociale;
- burse de ajutor social ocazional;
- burse mediu rural;
- burse pentru sportivii de performanță;
- subvenții individuale de sprijin pentru cazare (OUG 73/2004);
- decontarea biletelor de transport auto urban și interurban, în cuantum de 50% din valoare.

40.	Cum pot face autentificarea actelor de studii universitare?
R. La link-ul https://cnred.edu.ro/ro/vizarea-actelor-de-studii-pentru-plecarea-in-strainatate se regăsesc toate documentele necesare precum si procedura de autentificare a actelor de studii. 

41.	Se poate face un transfer, de la o specializare in cadrul facultății la alta specializare in anul 3 de studii?
R. Studentul poate urma o altă specializare dacă la sfârșitul unui an de studiu a obținut 60 credite în acel an și a susținut un nou concurs de admitere. In acest caz, în contractul de studii se va stipula printr-o clauză specială că toate  disciplinele care apar ca urmare a schimbării specializării vor trebui promovate în anul următor, în plus fată de sarcinile anuale minime ale noii specializări. Dacă această clauză nu este îndeplinită studentul revine la vechea specializare, pierzând, deci, un an. 
În mod excepțional transferul la altă specializare din cadrul facultății mai este posibil o singură dată și în cadrul Competiției de ocupare a locurilor bugetate rămase libere prin pierderile de școlaritate.
Nu se admite transferul studenților bugetați în exteriorul facultății.

42.	Cum se poate face întreruperea studiilor?
R. Întreruperea studiilor se poate face numai la începutul anului universitar în perioada în care se încheie contractele de studii. Numărul de întreruperi și durata acestora sunt în concordanță cu reglementările legale în vigoare. Întreruperea studiilor se poate face și în timpul anului universitar dacă există motive independente de voința studentului (certificat medical prelungit, caz de forță majoră).
 


