"""NWA URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views
from django.http import Http404
from django.shortcuts import render


urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login_register/', views.login_register),
    path('login_register/postsign', views.home),
    path('faq/', views.faq),
    path('contact/', views.contact),
    path('about/', views.about),
    path('nav_template/', views.nav_template),
    path('register/', views.register),
    path('signin/', views.sigin),
    path('logged_out/', views.logged_out),
    path('logout/', views.logout),
    path('add_question/', views.add_question),
    path('add_category/', views.add_category),
    path('show_qa/', views.show_qa),
    path('',views.home)
    
]