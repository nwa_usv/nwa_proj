class Question:
    id = ""
    category_id = ""
    answer_id = ""
    text = ""
    author = ""
    date = ""
    __answer__ = ""

    def get_answer(self):
        return self.__answer__

    def set_answer(self, answer):
        self.__answer__ = answer