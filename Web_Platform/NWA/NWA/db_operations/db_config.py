import pyrebase	# firebase API wrapper

################################################################################
# firebase connection config
config = {'apiKey': "AIzaSyCihIm5mZ8wbu6_N76-CCsyG6CazgimJ14",
    'authDomain': "nwa-qa-secretary.firebaseapp.com",
    'databaseURL': "https://nwa-qa-secretary.firebaseio.com",
    'projectId': "nwa-qa-secretary",
    'storageBucket': "nwa-qa-secretary.appspot.com",
    'messagingSenderId': "820374842260"
}
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
database = firebase.database()

current_user = {"id": "", "email": ""}
################################################################################