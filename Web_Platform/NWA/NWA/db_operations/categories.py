from NWA.db_operations.db_config import database

from NWA.qa_models import Category
from collections import OrderedDict

def load_categories():
    categ_table = database.child("categories").get().val()
    categs = list()
    for categ_id in categ_table.keys():
        new_cat = Category.Category(categ_id, categ_table[categ_id]["text"])
        categs.append(new_cat)
    return categs


def do_add_category(request):
    text = request.POST.get("new_category")
    data = {
        "text": text
    }
    database.child("categories").set(data)
    q = list()
    categories = load_categories()
    return render(request, "start-admin.html", {"questions": q,"options": categories})