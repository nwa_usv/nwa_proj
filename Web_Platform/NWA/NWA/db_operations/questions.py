from django.shortcuts import render
import datetime
from datetime import datetime
from django.utils import formats

from NWA.db_operations.db_config import database
from NWA.db_operations.db_config import current_user
from NWA.db_operations.categories import load_categories

from NWA.qa_models import Category
from NWA.qa_models import Question
from NWA.qa_models import Answer


def do_show_qa(request):
    q = load_questions()
    answers_table = database.child("answers").get().val()
    for question in q:
        question.set_answer(answers_table.get(str(question.answer_id))["text"])

    return render(request, "faq.html", {"questions": q})

def load_questions():
    questions_table = database.child("questions").get().val()
    users_table = database.child("users").get().val()
    categories_table = database.child("categories").get().val()
    questions = list()
    for question_id in questions_table.keys():
        try:
            question                = Question.Question()
            question.id             = question_id
            question.text           = questions_table[question_id]["text"].upper()
            question.author         = users_table[questions_table[question_id]["author"]]["email"]
            question.date           = questions_table[question_id]["date"].split(" ")[0]
            question.answer_id      = questions_table[question_id]["answer_id"]
            question.category_id    = categories_table[questions_table[question_id]["category_id"]]["text"]

            questions.append(question)
        except :
            return questions
    return questions

def do_add_question(request):
    if "add_category" in request.POST:
        text = request.POST.get("new_category")
        data = {
            "text": text
        }
        new_cat = Category.Category(hash(text), text)
        database.child("categories").child(new_cat.id).set(data)
    elif "add_question" in request.POST:
        print(str(request.POST))
        text_q = request.POST.get('question')
        text_a = request.POST.get('answer')
        categories = load_categories()
        category_id = 0
        for category in categories:
            if category.name == request.POST.get('category'):
                category_id = category.id
                break

        now = datetime.now()

        answer = Answer.Answer()
        answer.text = text_a
        answer.id = hash(text_a)
        data = {
            "text": answer.text
        }
        database.child("answers").child(answer.id).set(data)

        question = Question.Question()
        question.date = now
        question.text = text_q
        question.author = "Fe6TX0TVIFPbg1yADPG7zIaWao02"
        question.answer_id = answer.id
        question.category_id = category_id
        question.id = hash(hash(question.text) + hash(question.date))

        data = {
            "text": question.text,
            "author": question.author,
            "date": str(question.date),
            "answer_id": question.answer_id,
            "category_id": category_id
        }
        database.child("questions").child(question.id).set(data)

    categories = load_categories()
    # q = load_questions()
    q = list()
    return render(request, "start-admin.html", {"role": "admin", "questions": q, "categories": categories})