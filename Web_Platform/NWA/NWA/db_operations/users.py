"""
This file is intended to save the users and user methods.

"""
from django.shortcuts import render
import pyrebase	# firebase API wrapper


from NWA.db_operations.db_config import auth
from NWA.db_operations.db_config import database
from NWA.db_operations.db_config import current_user
from NWA.db_operations.categories import load_categories

################################################################################
## Firesbase configuration config array
config = {'apiKey': "AIzaSyCihIm5mZ8wbu6_N76-CCsyG6CazgimJ14",
    'authDomain': "nwa-qa-secretary.firebaseapp.com",
    'databaseURL': "https://nwa-qa-secretary.firebaseio.com",
    'projectId': "nwa-qa-secretary",
    'storageBucket': "nwa-qa-secretary.appspot.com",
    'messagingSenderId': "820374842260"
}
## Firesbase initialization
firebase = pyrebase.initialize_app(config)
## Firesbase authentication
auth = firebase.auth()
## Firesbase database configuration
database = firebase.database()
################################################################################

################################################################################
def do_sign_in(request):
    """ /* Function to sign in the user */"""
    email = request.POST.get("email")
    password = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email, password)
    except:
        message = "Invalid credentials"
        return render(request, "register.html", {"msg": message},{'nbar': 'register'})

    current_user["id"] = user["localId"]
    role = database.child("roles").order_by_child("user_id").equal_to(current_user["id"]).get()
    try:
        key = "user_" + current_user["id"]
        role = role.val()[key]["role"]
    except:
        role = "user"
    print(str(role))
    request.session['role_'] = role
    request.session['author'] = user["localId"]
    if role == "user":
        request.session['logat'] = 0
        request.session['username'] = email
        return render(request, "start.html", {"e": email, "role": role})
    else:
        q = list()
        categ = load_categories()
        request.session['logat'] = 1
        return render(request, "start-admin.html", {"role": role, "questions": q,"categories": categ})

################################################################################

def do_logout(request):
    """ /* Function to logout the user */"""
    request.session['role_'] = "none"
    try:
        user = auth.sign_in_with_email_and_password(email, password)
        auth.signOut(user)
        email = None
        password = None
        user = None
    except:
        message = "Problem signing out !"
        request.session['logat'] = -1
        request.session['username'] = None
        return render(request, "logged_out.html", {"msg": "Delogare cu succes ! "})
    request.session['logat'] = 'none'
    return render(request, "logged_out.html", {"msg": "Delogare cu succes ! "})
################################################################################

################################################################################
def do_register(request):
    """ /* Function to register the user */ """
    first_name = request.POST.get("first_name")
    last_name = request.POST.get("last_name")
    email = request.POST.get("email")
    password = request.POST.get("pass")
    try:
        if email is None or not str.endswith(email, "@student.usv.ro"):
            nbar = "register"
            raise Exception("Te rugam sa introduce o adresa de tipul : @student.usv.ro")

        user = auth.create_user_with_email_and_password(email, password)
        user_id = user["localId"]
        data = {
            "first_name": first_name,
            "email": email,
            "last_name": last_name
        }
        database.child("users").child(user_id).set(data)
        return do_sign_in(request)
    except Exception as e:
        print("Register error exception", type(e))
        if hasattr(e, 'response'):
            response = e.args[0].response
            error = response.json()['error']
            message = error["message"]
        else:
            message = str(e)
        return render(request, "register.html", {"msg": message},{'nbar': 'register'})
################################################################################
